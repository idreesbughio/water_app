import 'package:flutter/material.dart';
import 'package:sensors/sensors.dart'
    show accelerometerEvents, AccelerometerEvent;
import 'dart:async';
import 'dart:math';
import 'dart:math' as math;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: LiquidAnimation(),
    );
  }
}

class LiquidAnimation extends StatefulWidget {
  LiquidAnimationState createState() => LiquidAnimationState();
}

const _numberOfWavePoints = 11;

class LiquidAnimationMap {
  List<double> heightMap;
  List<double> velocityMap;

  LiquidAnimationMap({this.heightMap, this.velocityMap});

  updateMaps(double timeInterval) {
    const _speed = 15.0;
    const _acceleration = 15.0;
    const _dampening = 0.9;

    double edgeness = 0;
    int a = _numberOfWavePoints - 3;
    const double _edgeMultiplier = 0.07;

    for (int i = 1; i < _numberOfWavePoints - 1; i++) {
      edgeness = (((2 * i - (2 + a)).toDouble()) / a.toDouble()).abs();

      velocityMap[i] += _acceleration *
          timeInterval *
          (((heightMap[i - 1] + heightMap[i + 1]) / 2.0) - heightMap[i]);
      velocityMap[i] *= pow(_dampening * (1 + _edgeMultiplier * edgeness),
          timeInterval * _acceleration);
    }

    double maxHeight = 0.0;

    for (int i = 1; i < _numberOfWavePoints - 1; i++) {
      heightMap[i] += _speed * timeInterval * velocityMap[i];
      maxHeight = max(maxHeight, heightMap[i].abs());
    }

    if (maxHeight > 1.0) {
      for (int i = 0; i < _numberOfWavePoints; i++) {
        heightMap[i] = heightMap[i] / maxHeight;
      }
    }
  }

  void disturb(double velocity, double point) {
    int index = (point * (_numberOfWavePoints - 3)).toInt() + 1;
    velocityMap[index] += velocity;
  }

  double maxVelocity() {
    return velocityMap.map((element) => element.abs()).reduce(max);
  }
}

class LiquidAnimationState extends State<LiquidAnimation>
    with SingleTickerProviderStateMixin {
  LiquidAnimationMap _animationMap = LiquidAnimationMap(
    heightMap: List.filled(_numberOfWavePoints, 0),
    velocityMap: List.filled(_numberOfWavePoints, 0),
  );
  List<double> _accelerometerValues;
  DateTime _lastTimestamp = DateTime.now();
  Timer _timer;

  double _acceleration = 0;
  final _velocityFactor = 10.0;

  AnimationController _rotationController;

  @override
  void initState() {
    _timer = Timer.periodic(Duration(milliseconds: 16), ((Timer timer) {
      var now = DateTime.now();
      var interval =
          (now.millisecondsSinceEpoch - _lastTimestamp.millisecondsSinceEpoch)
                  .toDouble() /
              1000;

      var velocity = (_acceleration * interval * _velocityFactor).abs();
      var disturbancePoint = _acceleration > 0 ? 0 : 1;

      setState(() {
        _animationMap.disturb(velocity, disturbancePoint.toDouble());
        _animationMap.updateMaps(interval);
        _lastTimestamp = now;
      });
    }));
    accelerometerEvents.listen((AccelerometerEvent event) {
      const GRAVITY = 9.8;
      _acceleration = -event.x / GRAVITY;
      _accelerometerValues = <double>[event.x, event.y, event.z];
    });

    _rotationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 300),
    );

    super.initState();
  }

  int incriment = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          FloatingActionButton(
            backgroundColor: Colors.orangeAccent,
            elevation: 0.0,
            child: Icon(Icons.add),
            onPressed: () {
              setState(() {
                if (incriment < 10) {
                  incriment++;
                  _animationMap.disturb(0.1, 1);
                  _animationMap.updateMaps(0.1);
                }
              });
            },
          ),
          SizedBox(width: 20),
          FloatingActionButton(
            backgroundColor: Colors.orangeAccent,
            elevation: 0.0,
            child: Icon(Icons.remove),
            onPressed: () {
              setState(() {
                if (incriment > 0) {
                  incriment--;
                  _animationMap.disturb(0.1, 1);
                  _animationMap.updateMaps(0.1);
                }
              });
            },
          ),
        ],
      ),
      body: Stack(
        children: <Widget>[
          AnimatedContainer(
            duration: Duration(milliseconds: 300),
            transform: Matrix4.rotationZ((_accelerometerValues[1] > 0)
                ? pi * (_accelerometerValues[0] / (9.81 * 2))
                : -pi * (_accelerometerValues[0] / (9.81 * 2)) - pi)
              ..translate(
                  0.0,
                  MediaQuery.of(context).size.height -
                      (MediaQuery.of(context).size.height / 10) * incriment,
                  0.0)
              ..scale(2.0, 1.0, 1.0),
            alignment: FractionalOffset.centerLeft,
            // origin: Offset(
            //   MediaQuery.of(context).size.width / 2,
            //   MediaQuery.of(context).size.height / 2,
            // ),
            child: CustomPaint(
              painter: LiquidPainter(map: _animationMap),
              child: Center(
                child: Text(""),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }
}

class LiquidPainter extends CustomPainter {
  LiquidPainter({this.map});
  final LiquidAnimationMap map;
  final _waveHeight = 50.0;

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint();
    var surfacePath = _path(Size(size.height, size.height));

    paint.color = Colors.blueAccent;
    var bodyPath = surfacePath;
    bodyPath.lineTo(size.height, size.height);
    bodyPath.lineTo(0, size.height);
    bodyPath.close();

    canvas.drawPath(bodyPath, paint);

    paint.color = Colors.blue;
    paint.strokeWidth = 2.0;
    paint.style = PaintingStyle.stroke;

    canvas.drawPath(surfacePath, paint);
  }

  Path _path(Size size) {
    double pointDistance = size.width / (_numberOfWavePoints - 3);

    Path path = Path();
    Point _cp = Point(-pointDistance, _yValue(map.heightMap[0]));

    path.moveTo(_cp.x, _cp.y);

    double edgeness = 0;
    int a = _numberOfWavePoints - 3;
    const double edgeMultiplier = 0.5;

    for (int x = 1; x < _numberOfWavePoints; x++) {
      edgeness = (((2 * x - (2 + a)).toDouble()) / a.toDouble()).abs();

      double y = _yValue(map.heightMap[x] * (1 + edgeMultiplier * edgeness));

      Point next = Point(_cp.x + pointDistance, y);
      Point cp1 = Point(_cp.x + pointDistance * 0.5, _cp.y);
      Point cp2 = Point(_cp.x + pointDistance * 0.5, y);

      path.cubicTo(cp1.x, cp1.y, cp2.x, cp2.y, next.x, next.y);
      _cp = next;
    }

    return path;
  }

  double _yValue(double height) {
    return (1 - min(height, 1)) * _waveHeight;
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
